var request = require('supertest'); 

var app = require('../server.js');


describe('GET /', function() {
    it('displays "Hello Black World !"',
    function(done) { // The line below is the core test of our app.     
        request(app).get('/').expect('Hello Black World !', done);
    });
});
